import Box from "@mui/material/Box";

import { Layout } from "./components/Layout";

const App = () => {
  return (
    <Box>
      <Layout />
    </Box>
  );
};

export default App;

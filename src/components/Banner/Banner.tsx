import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

export const Banner = () => {
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        paddingY: 10,
        backgroundColor: "lightseagreen",
        color: "white",
        width: "100%",
        gap: 2,
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <Typography variant="h3">A joke a day keeps the doctor away</Typography>
      <Typography variant="h6">
        If you joke wrong way, your teeth have to pay. (Serious)
      </Typography>
    </Box>
  );
};

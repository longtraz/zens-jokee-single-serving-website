import { useState } from "react";

import { Button } from "../Button";
import { CircularProgress, Typography } from "@mui/material";

import Box from "@mui/material/Box";
import { useJoke } from "../../hook";

export const Body = () => {
  const [jokeIndex, setJokeIndex] = useState(0);
  const { data, loading } = useJoke();

  const isRunOutOfJoke = jokeIndex >= data.length;

  const changeJokeStatus = async (status: string) => {
    await fetch("http://localhost:8080/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ id: jokeIndex, status }),
    });
  };

  const renderJoke = () => {
    if (loading)
      return (
        <Box sx={{ display: "flex" }}>
          <CircularProgress />
        </Box>
      );

    if (isRunOutOfJoke) {
      return (
        <Box sx={{ display: "flex", justifyContent: "center" }}>
          <Typography variant="h4" color="GrayText">
            "That's all the jokes for today! Come back another day!"
          </Typography>
        </Box>
      );
    }

    return (
      <Typography variant="h6" color="GrayText">
        {data[jokeIndex].content}
      </Typography>
    );
  };
  return (
    <Box
      sx={{
        display: "flex",
        alignContent: "center",
        flexDirection: "column",
        paddingX: 40,
        paddingY: 10,
        gap: 10,
      }}
    >
      {renderJoke()}
      {!isRunOutOfJoke && (
        <Box sx={{ display: "flex", justifyContent: "center", gap: 10 }}>
          <Button
            children={<Typography variant="h5">"This is Funny!"</Typography>}
            sx={{ backgroundColor: "blue" }}
            onClick={async () => {
              await changeJokeStatus("upvote");
              setJokeIndex(jokeIndex + 1);
            }}
          />
          <Button
            children={
              <Typography variant="h5">"This is not funny."</Typography>
            }
            sx={{ backgroundColor: "green" }}
            onClick={async () => {
              await changeJokeStatus("downvote");
              setJokeIndex(jokeIndex + 1);
            }}
          />
        </Box>
      )}
    </Box>
  );
};

import ButtonBase, { ButtonBaseProps } from "@mui/material/ButtonBase";

export const Button: React.FC<ButtonBaseProps> = ({
  children,
  sx,
  ...rest
}) => (
  <ButtonBase sx={{ width: 400, paddingY: 2, color: "white", ...sx }} {...rest}>
    {children}
  </ButtonBase>
);

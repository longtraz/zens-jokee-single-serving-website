import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";

import zensLogo from "../../assets/zensLogo.png";
import author from "../../assets/author.png";

export const Header = () => {
  return (
    <AppBar position="static" color="transparent" sx={{ boxShadow: "none" }}>
      <Box
        sx={{
          display: "flex",
          paddingX: 30,
          gap: 2,
          alignItems: "center",
        }}
      >
        <Box sx={{ flex: 1 }}>
          <img src={zensLogo} alt="Zens Logo" height="50" />
        </Box>

        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
          }}
        >
          <Typography variant="body2" noWrap color="GrayText">
            Handicrafted by
          </Typography>
          <Typography variant="body1" noWrap>
            Long Tran
          </Typography>
        </Box>

        <Avatar alt="Author" src={author} />
      </Box>
    </AppBar>
  );
};

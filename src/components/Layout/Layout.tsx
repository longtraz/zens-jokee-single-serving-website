import { Box } from "@mui/material";
import { Header } from "../Header";
import { Banner } from "../Banner";
import { Body } from "../Body";

export const Layout = () => {
  return (
    <Box sx={{ width: "100vw", overflowX: "hidden" }}>
      <Header />
      <Banner />
      <Body />
    </Box>
  );
};

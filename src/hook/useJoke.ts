import { useEffect, useState } from "react";
import { Joke } from "../types/joke";

export const useJoke = () => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<Joke[]>([]);

  const fetchJokeList = async () => {
    setLoading(true);

    const res = await fetch("http://localhost:8080/");
    const jokeList: Joke[] = await res.json();

    setData(jokeList);
    setLoading(false);
  };

  useEffect(() => {
    fetchJokeList();
  }, []);

  return { loading, data };
};

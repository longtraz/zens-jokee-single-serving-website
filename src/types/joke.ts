export interface Joke {
  id: number;
  content: string;
  status: string;
  createdAt: string;
  updatedAt: string;
}
